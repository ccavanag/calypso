#ifndef XAODFASERTRACKING_XAODPRIMITIVES_H
#define XAODFASERTRACKING_XAODPRIMITIVES_H

#include <unistd.h>
#include <Eigen/Core>
#include <Eigen/Dense>

namespace  FMath{
  template <int N>
  inline void compress(const Eigen::Matrix<double, N,N,0,N,N>& covMatrix, std::vector<float>& vec) {
      int rows = covMatrix.rows();
      for (int i = 0; i < rows; ++i) {
          for (int j = 0; j <= i; ++j) {
              vec.push_back(covMatrix(i, j));
          }
      }
  }
  
  template <int N>
  inline void expand(std::vector<float>::const_iterator it,
          std::vector<float>::const_iterator it_end, Eigen::Matrix<double,  N,N,0,N,N>& covMatrix) {
      unsigned int dist = std::distance(it, it_end);
      unsigned int n;
      for (n = 1; dist > n; ++n) {
          dist = dist - n;
      }
      covMatrix = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>(n, n);
      for (unsigned int i = 0; i < n; ++i) {
          for (unsigned int j = 0; j <= i; ++j) {
              covMatrix(i,j) = *it;
              ++it;
          }
      }
  }
}

namespace xAOD {
    
  using DefiningParameters_t  = Eigen::Matrix<double, 5, 1>;
  using ParametersCovMatrix_t = Eigen::Matrix<double, 5, 5>;
  using CurvilinearParameters_t = Eigen::Matrix<double, 6, 1>;
  
    /// Enums to identify who created this track and which properties does it have.
  enum FaserTrackFitter
  {
      ///Track fitter not defined.
      Unknown        = 0,
      ///tracks produced by the Kalman Fitter
      KalmanFitter       = 1,
      ///maximum number of enums
      NumberOfTrackFitters     = 2
  };

  enum FaserParticleHypothesis { nonInteracting  = 0,
    geantino        = 0,
    electron        = 1,
    muon            = 2,
    pion            = 3,
    kaon            = 4,
    proton          = 5,
    noHypothesis    = 99,
    undefined       = 99
  };

  /// Enum allowing us to know where in FASER the parameters are defined.
  enum FaserParameterPosition {
    /// Parameter defined at the first plane.
    FirstPlane,
    /// Parameter defined at the position of the 1st measurement.
    FirstMeasurement,
    /// Parameter defined at the position of the last measurement.
    LastMeasurement,
    /// Parameter defined at the entrance to the calorimeter.
    CalorimeterEntrance,
    /// Parameter defined at the exit of the calorimeter.
    CalorimeterExit
  };


  /// Enumerates the different types of information stored in Summary. 
  /// Please note that the values have specific types - i.e. some are float, whilst most are uint8_t.
  /// When adding a new transient information type, please make sure to increase numberOfTrackSummaryTypes.*/
  enum FaserSummaryType {
    // --- Inner Detector
    numberOfContribStripLayers          = 1,  //!< number of contributing layers of the pixel detector [unit8_t].
    numberOfStripHits                 = 2,  //!< number of hits in Strip [unit8_t].
    numberOfStripOutliers             = 3,  //!< number of Strip outliers [unit8_t].
    numberOfStripHoles                = 4,  //!< number of Strip holes [unit8_t].
    numberOfStripDoubleHoles          = 5,  //!< number of Holes in both sides of a Strip module [unit8_t].
    numberOfStripSharedHits           = 6,  //!< number of Strip hits shared by several tracks [unit8_t].
    numberOfStripDeadSensors          = 7,  //!< number of dead Strip sensors crossed [unit8_t].
    numberOfStripSpoiltHits           = 8,  //!< number of Strip hits with broad errors (width/sqrt(12)) [unit8_t].

    // --- all
    numberOfOutliersOnTrack           = 9,  //!< number of measurements flaged as outliers in TSOS [unit8_t].
    standardDeviationOfChi2OS         = 10, //!< 100 times the standard deviation of the chi2 from the surfaces [unit8_t].
    // -- numbers...
    numberOfTrackSummaryTypes         = 11
  };
  
} //  namespace xAOD 

#endif // XAODFASERTRACKING_XAODPRIMITIVES_H