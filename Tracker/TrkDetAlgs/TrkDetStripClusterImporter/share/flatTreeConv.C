
#include <vector>

#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"

#include "TInterpreter.h"

void read()
{
   gInterpreter->GenerateDictionary("vector<vector<float> >","vector");
   TFile *f = TFile::Open("/afs/cern.ch/work/r/rjansky/FASER/clusters.root","READ");

   if (!f) { return; }
   TTree *t = (TTree*)f->Get("clusters");
   Int_t event_nr;  
   Float_t g_x,g_y,g_z,l_x,l_y;

   t->SetBranchAddress("event_nr",&event_nr);
   t->SetBranchAddress("g_x",&g_x);
   t->SetBranchAddress("g_y",&g_y);
   t->SetBranchAddress("g_z",&g_z);
   t->SetBranchAddress("l_x",&l_x);
   t->SetBranchAddress("l_y",&l_y);
      
   TFile *fNew = TFile::Open("clustersVec.root","RECREATE");

   std::vector<float> g_xV,g_yV,g_zV,l_xV,l_yV;

   // Create a TTree
   TTree *tNew = new TTree("clusters","Tree with clusters");
   tNew->Branch("g_x",&g_xV);
   tNew->Branch("g_y",&g_yV);
   tNew->Branch("g_z",&g_zV);
   tNew->Branch("l_x",&l_xV);
   tNew->Branch("l_y",&l_yV);
   
   
   int prevEvent=0;
   for (Long64_t  i = 0; i < t->GetEntries(); i++) {

      t->GetEntry(i);
      
      if(prevEvent!=event_nr&&i!=0){
          tNew->Fill();
          g_xV.clear();
          g_yV.clear();
          g_zV.clear();
          l_xV.clear();
          l_yV.clear();
          std::cout << "clearing: " << i <<std::endl;
      }
      prevEvent=event_nr;
      g_xV.push_back(g_x);
      g_yV.push_back(g_y);
      g_zV.push_back(g_z);
      l_xV.push_back(l_x);
      l_yV.push_back(l_y);
   }
   fNew->Write();
   delete fNew;
}


void flatTreeConv()
{
   read();
} 