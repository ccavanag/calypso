atlas_subdir(MCEvents)

atlas_add_component(
        MCEvents
        src/MCEventsAlg.h
        src/MCEventsAlg.cxx
        src/components/MCEvents_entries.cxx
        LINK_LIBRARIES AthenaBaseComps GeneratorObjects StoreGateLib TrackerIdentifier TrackerSimEvent GaudiKernel)
atlas_install_python_modules(python/*.py)
atlas_install_scripts(test/*.py)
